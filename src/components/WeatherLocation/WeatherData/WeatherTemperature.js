import React from 'react';
import WeatherIcons from 'react-weathericons';
import PropTypes from 'prop-types';
import{
    SUN,
    FOG,
    CLOUD,
    CLOUDY,
    RAIN,
    SNOW ,
    WINDY,
    THUNDER,
    DRIZZLE,
} from './../../../constants/weathers'

import './styles.css';

const icons = {
    [SUN]: "day-sunny",
    [FOG]:"day-fog",
    [CLOUD]:"cloud",
    [CLOUDY]:"cloudy",
    [RAIN]:"rain",
    [SNOW]: "snow",
    [WINDY]:"windy",
    [THUNDER]:"day-thunderstorm",
    [DRIZZLE]:"day-showers",
}

const getWeatherIcon = weatherState =>{
    const icon = icons[weatherState];

    const sizeIcon = "2x";

    if(icon)
        return <WeatherIcons className="wicon" name={icon} size={sizeIcon}></WeatherIcons>
    else
        return <WeatherIcons className="wicon" name={"day-sunny"} size={sizeIcon}></WeatherIcons>

}

const WeatherTemperature = ({ temperature, weatherState}) =>(
    <div className="weatherTemperatureCont">{
        getWeatherIcon(weatherState)
    }
        <span className="temperature">{`${temperature}`}</span>
        <span className="temperatureType">{ "°C"}</span>
        </div>
);

WeatherTemperature.propTypes = {
    temperature: PropTypes.number.isRequired,
    weatherState: PropTypes.string.isRequired,
};

export default WeatherTemperature;