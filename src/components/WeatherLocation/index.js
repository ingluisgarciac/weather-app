import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress'
import PropTypes from 'prop-types'
import Location from './Location';
import WeatherData from './WeatherData';
import './styles.css';

import {
    // eslint-disable-next-line
    SUN,
    //FOG,
    //CLOUD,
    //CLOUDY,
    //RAIN,
    //SNOW ,
    //WINDY,
} from './../../constants/weathers';
//import getUrlWeatherByCity from '../../services/getUrlWeatherByCity';

/*
handleUptdateClick = () => {
        const api_weather= getUrlWeatherByCity(this.state.city);
        fetch(api_weather).then(resolve => {
            return resolve.json();
        }).then(data => {
            const newWeather = transformWeather(data);
            this.setState({
                data: newWeather,
            });
        });

    }

    constructor(props) {
        super(props);
        const {city} = props;
        this.state = {
            city,
            data: null,
        };
       
    }

    

    componentDidMount() {
        
        this.handleUptdateClick();
    }

    componentDidUpdate(prevProps, prevState) {
        
    }


*/


const WeatherLocation =({onWeatherLocationClick, city, data}) =>(

    
        <div className="weatherLocationCont" onClick={onWeatherLocationClick}>
            <Location city={city}></Location>
            {data ?
                <WeatherData data={data} /> :
                <CircularProgress />
                }
        </div>
        
    
);

WeatherLocation.propTypes ={
    city: PropTypes.string.isRequired,
    onWeatherLocationClick: PropTypes.func,
    data: PropTypes.shape({
        temperature: PropTypes.number.isRequired,
        weatherState: PropTypes.string.isRequired,
        humidity: PropTypes.number.isRequired,
        wind: PropTypes.string.isRequired,
    }),
}

export default WeatherLocation;